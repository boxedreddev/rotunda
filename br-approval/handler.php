<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
Tested working with PHP5.4 and above (including PHP 7 )

 */
require_once './vendor/autoload.php';

use FormGuide\Handlx\FormHandler;

$pp = new FormHandler();

$validator = $pp->getValidator();
$validator->fields(['name','email','company','approved'])->areRequired()->maxLength(75);
$validator->field('email')->isEmail();
// $validator->field('comments')->maxLength(6000);

// change this to the account manager //
$pp->sendEmailTo('ruth@boxedred.com');

echo $pp->process($_POST);
