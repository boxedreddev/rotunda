<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Website Design Approval Form | BoxedRed Marketing</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
      <link rel="stylesheet" href="form.css" >
      <script src="form.js"></script>
  </head>
    <body>
      <header>
        <img src="imgs/boxedred-logo.png">
      </header>
      <div class="container">

        <!-- form -->
        <div class="form_Wrap">

          <h1 class="header_Title">Your website design is ready to be built!</h1>

          <p class="inner_Content">
            We just need a digital signature below to confirm you are 100% happy with the design and are ready to see it built.<br><br>
            Any design changes after this form has been submitted will be subject to a charge. You will have <strong>one round of snags</strong> once the build first version has been completed and sent to you on this staging domain. You can compile <strong>one document</strong> that we will implement as part of your build, these snags should only be about the approved designs and site functionality, <em>if we deem them to be additional to the original design or scope they will be quoted for accordingly</em>.<br><br>
            Once you press ‘submit’ below, we will email you with confirmation and start our development process. <br><br>
          </p>
          <!-- form -->
          <form id="reused_form">

            <div class="innerFW">
              <label >Name</label>
              <input type="text" name="name" required>
            </div>

            <div class="innerFW">
              <label>Company Name</label>
              <input type="text" name="company" required>
            </div>

            <div class="innerFW">
              <label>Email Address</label>
              <input type="email" name="email" required>
            </div>

            <div class="innerFWckbx">
              <?php $host = isset($_SERVER['SERVER_NAME'])?$_SERVER['SERVER_NAME']:'localhost'; ?>
              <input type="checkbox" name="approved" required value="I have checked over the design and confirm it is ready to be built."> I confirm I am happy with the deisign and it is ready go into development.<br>
            </div>

            <div class="innerFW">
              <button class="btn btn-styled" type="submit">Submit</button>
            </div>

          </form>

          <!-- // END - form -->

          <footer>
            <?php
            // Prints the day
            echo date("D d M Y");

            ?>
          </footer>
        </div>

      </div>

    </body>
</html>
