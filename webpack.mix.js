const mix = require('laravel-mix');
// var setup //
const DEV_DOMAIN = 'rotunda.local';
// theme & dir //
var theme = 'rotunda';
var themeDir = 'wp-content/themes/'+theme+'/';

mix.combine([
  themeDir + 'library/js/libs/aos.js',
  themeDir + 'library/js/libs/modernizr.custom.min.js',
  themeDir + 'library/js/libs/owl.carousel.min.js',
  themeDir + 'library/js/libs/smoothScroller.js',
  themeDir + 'library/js/scripts.js'
], themeDir + 'library/js/scripts.min.js');


mix.sass( themeDir + 'library/scss/style.scss', themeDir + 'library/css/')
.sourceMaps(mix.inProduction(), 'source-map')
mix.options({ processCssUrls: false })

.browserSync({
  watchOptions: {
      ignored: /node_modules/
    },
  open: false,
  proxy: DEV_DOMAIN,
  files: [ themeDir + 'library/css/style.css', themeDir + '**/*.php', themeDir + 'library/js/scripts.min.js'],
})
