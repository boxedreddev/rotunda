<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

require_once(ABSPATH . 'config_setup.php');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $db_config[$env]['database']);

/** MySQL database username */
define('DB_USER', $db_config[$env]['username']);

/** MySQL database password */
define('DB_PASSWORD', $db_config[$env]['password']);

/** MySQL hostname */
define('DB_HOST', $db_config[$env]['host']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')a/.a(JLrq%}$n(@@-Uv*0Bam$D!`@myR(?D$EHVt`^4|RBv-}Lph~l:goS.T;-L');
define('SECURE_AUTH_KEY',  '*-2%}iIY|;]#v`-|U^{CJ4CXEdp>9}[>Z f^314OwD+=Pv xaJpu1R{NC[JhiC+8');
define('LOGGED_IN_KEY',    ')V393hj}V-}{3YcyB)`jnFZcV+|!4l^?5rNEnw!uP -p{ghy{I+.*V(dev||1D-W');
define('NONCE_KEY',        'F4Bg~OBuLBrpwtwI2zJAzOn^:9t{DC 0_$pd:QZn$<(C/&5Q+gSv`-&~+bV1!pXg');
define('AUTH_SALT',        'lovI7hUOs;KRfLSE&r{ |=LRj`pCPAzu@o{pp@E_C ;Y?`fJ*H-0z-$=CK$y&Cv+');
define('SECURE_AUTH_SALT', '4YaC||?W%h(`#pi`MM||IJeW&EduPW%B$L9~+zG0s^5.1SC,G>yvi<-UI3uh<oEb');
define('LOGGED_IN_SALT',   ':X {qZ>Za|Hx<M?gsW+SEFyyyA-Y!N8.P#w|T9bgg}9<yGV-=4BXFif?!}gbfy[u');
define('NONCE_SALT',       '`Sqsn5J`*nPO}V>Iq1b0qor{=g|c*_UIa jR(FBsy2Il>8,%~%+7,;i7::|@$!C=');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '64M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
