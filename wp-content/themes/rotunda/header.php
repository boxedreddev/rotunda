<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?></title>
		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<!-- Favicons -->
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri()?>/library/images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri()?>/library/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri()?>/library/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri()?>/library/images/favicon/site.webmanifest">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/library/images/favicon/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
    <meta name="theme-color" content="#121212">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php
		// wordpress head functions
	 	wp_head();
		// check host - get analytics tracking if not local/staging //
		$host = $_SERVER['HTTP_HOST'];
		if($host == "www.livedomain.com" or $host == "livedomain.com") {
			// tracking //
			get_template_part('includes/tracking/ga-tracking');
		}
		?>
	</head>
	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
	<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
		<div class="container">
			<a href="<?php echo url();?>">
				<img src="<?php echo image('boxedred-logo.svg');?>" alt="Logo Goes Here">
			</a>
			<nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
				<?php wp_nav_menu(array(
	         'container' => false,
	         'container_class' => 'menu cf',
	         'menu' => __( 'The Main Menu', 'startertheme' ),
	         'menu_class' => 'nav top-nav cf',
	         'theme_location' => 'main-nav',
	         'before' => '',
           'after' => '',
           'link_before' => '',
           'link_after' => '',
           'depth' => 0,
	         'fallback_cb' => ''
				)); ?>
			</nav>
		</div>
	</header>
