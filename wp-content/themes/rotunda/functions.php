<?php
// !! Where applicable always try and do things customly, plugins should be the last resort unless specified by client or really saves time !! //
// LOAD CORE and dependencies //
require_once( 'includes/core-functions.php' );
require_once( 'includes/helper-functions.php' );
require_once( 'includes/custom-functions.php' );

# =========== Local Update Fixes  =========== #
$host = $_SERVER['HTTP_HOST'];
if($host == "themestarter.local") { // replace with local URL //
  delete_option( "core_updater.lock" );
  add_filter('https_ssl_verify', '__return_false');
}

add_action ('wp_loaded', 'signoff_redirect');
function signoff_redirect() {
  if(get_field('redirect_traffic_signogf','options') && !is_admin()) {
		wp_redirect($host . '/br-approval');
		exit();
	}
}


# =========== Post Types =========== #
// require_once( 'includes/post-types/filename.php' ); // <-- pull in your post types like this here //
// This could be a post types file etc, require instead of blocking up the functions file - makes it easier to edit when needed //

# =========== Custom Functions =========== #
require_once( 'includes/custom-functions.php' );

# =========== Launch Starter Functions  =========== #
function bones_ahoy() {
  // change login sceen appearence //
  function wpb_login_logo() {
    echo '<link rel="stylesheet" type="text/css" href="'.get_template_directory_uri().'/library/css/login.css">';
  }
  add_action( 'login_enqueue_scripts', 'wpb_login_logo' );
  // let's get language support going, if you need it
  load_theme_textdomain( 'startertheme', get_template_directory() . '/library/translation' );
  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );
  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper
  // launching this stuff after theme setup
  bones_theme_support();
} /* end bones ahoy */

// let's get this party started //
add_action( 'after_setup_theme', 'bones_ahoy' );
# =========== Preset =========== #
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
/* DON'T DELETE THIS CLOSING TAG */
?>
