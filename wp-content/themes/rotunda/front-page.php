<?php
/*
  Front Page (home)
*/
get_header();
?>

<section id="dummyContent">
  <div class="container" data-aos="fade-up" style="text-align:center;">
    <h3>Example Carousel</h3>
    <div id="starterSlider" class="owl-carousel owl-theme">
      <div class="item" style="background-image:url('<?php echo get_template_directory_uri(); ?>/library/images/login-bg-screen.jpg');">
        <h4>Item 1</h4>
      </div>
      <div class="item" style="background-image:url('<?php echo get_template_directory_uri(); ?>/library/images/login-bg-screen.jpg');">
        <h4>Item 2</h4>
      </div>
      <div class="item" style="background-image:url('<?php echo get_template_directory_uri(); ?>/library/images/login-bg-screen.jpg');">
        <h4>Item 3</h4>
      </div>
      <div class="item" style="background-image:url('<?php echo get_template_directory_uri(); ?>/library/images/login-bg-screen.jpg');">
        <h4>Item 4</h4>
      </div>
    </div>
    <div class="overviewDep">
      <h2>Current Dependencies</h2>
      <ul>
        <li>Owl Carousel</li>
        <li>AOS for scroll effects</li>
        <li>Ajax url setup ready for requests</li>
        <li>SCSS compiler, just run npm run watch to compile js and scss files</li>
      </ul>
    </div>
  </div>
</section>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="container">
		<?php the_content(); ?>
	</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
