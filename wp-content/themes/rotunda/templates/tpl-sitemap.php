<?php
/*
 Template Name: Sitemap
*/

get_header(); ?>

<section id="html_sitemap_customTme">
	<div class="wrap white-section">
    <div id="Sitepages_wrap_br">
      <div class="site_left_br">
  		 <h4>Pages:</h4>
    		 <ul class="sitemap-pages">
    				 <?php wp_list_pages(array('exclude' => '', 'title_li' => '')); // Exclude pages by ID ?>
    		 </ul>
       </div>
       <div class="site_right_br">
    		 <h4>Posts:</h4>
  				 <?php
						$categories = get_categories('exclude='); // Exclude categories by ID
						foreach ($categories as $cat) {
						?>

						<ul class="cat-posts">
						<?php
							 query_posts('posts_per_page=-1&cat='.$cat->cat_ID); //-1 shows all posts per category. 1 to show most recent post.
							 while(have_posts()):
									 the_post();
									 $category = get_the_category();
									 if ($category[0]->cat_ID == $cat->cat_ID) { ?>
												 <li><a href="<?php the_permalink() ?>"><?php the_title();?></a></li>
									 <?php
									 }
							 endwhile;
						?>
						</ul>
				 <?php }
				  wp_reset_query(); ?>
       </div>
     </div>
	 </div>
 </section>


<?php get_footer(); ?>
