<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
	<div class="container">
		<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>
	</div>
</footer>
<?php // all js scripts are loaded in library/bones.php - don't inline style or script! ?>
<?php
wp_footer();
$host = $_SERVER['HTTP_HOST'];
if($host == "www.livedomain.com" or $host == "livedomain.com") {
	//conversions //
	get_template_part('includes/tracking/ga-conversions');
}
?>
</body>
</html><!-- end of site. what a ride! -->
