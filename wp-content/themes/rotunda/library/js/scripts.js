jQuery(document).ready(function($) {
  /* ====================== AOS ======================  */
  AOS.init();
  /* ====================== Owl ======================  */
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    items:1
  });
});
