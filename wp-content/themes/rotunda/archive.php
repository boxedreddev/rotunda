<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="container">
		<?php
			echo '<h3>' . get_the_title() . '</h3>';
			echo '<span>' . get_the_date() . '</span>';
      if(has_excerpt()) {
        echo '<p>'.wp_trim_words(get_the_excerpt(),'25','...').'</p>';
      }
      else {
        echo '<p>'.wp_trim_words(get_the_content(),'25','...').'</p>';
      }
    ?>
	</div>
<?php endwhile; endif; ?>

<!-- pagination -->
<?php if (function_exists('bones_page_navi')) {
		bones_page_navi();
	} else { ?>
	<nav class="wp-prev-next">
    <ul class="clearfix">
     <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "startertheme")) ?></li>
     <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "startertheme")) ?></li>
    </ul>
	</nav>
<?php } ?>

<?php get_footer(); ?>
